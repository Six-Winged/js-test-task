const gulp = require('gulp');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');

gulp.task ('walking-test', function () {
    return browserify ({entries: 'es6/walking.js', extensions: ['.js'], debug: true})
        .transform ('babelify', {
            presets: ['@babel/preset-env'],
            plugins: [["babel-plugin-inferno", {"imports": true}]],
        })
        .bundle ()
        .pipe (source('bundle-walking-dev.js'))
        .pipe (gulp.dest('dist/assets'));
});
gulp.task ('walking-production', function () {
    const envify = require ('envify/custom');
    return browserify ({entries: 'es6/walking.js', extensions: ['.js'], debug: false})
        .transform ('babelify', {
            presets: ['@babel/preset-env'],
            plugins: [["babel-plugin-inferno", {"imports": true}]],
        })
        .transform (
            // Required in order to process node_modules files
            {global: true},
            envify ({NODE_ENV: 'production'})
        )
        .bundle ()
        .pipe (source('bundle-walking-prod.js'))
        .pipe (gulp.dest('dist/assets'));
});

gulp.task ('hud-test', function () {
    return browserify ({entries: 'es6/nav.js', extensions: ['.js'], debug: true})
        .transform ('babelify', {
            presets: ['@babel/preset-env'],
            plugins: [["babel-plugin-inferno", {"imports": true}]],
        })
        .bundle ()
        .pipe (source('bundle-nav.js'))
        .pipe (gulp.dest('dist/assets'));
});