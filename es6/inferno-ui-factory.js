/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/26/18 8:51 PM
 */

import {WalkDirections} from './constants/walk-directions.js';
import GameButton from './view/inferno/GameButton.js';
import GameJoystick from "./view/inferno/GameJoystick.js";
import GamePlayerSwitcher from "./view/inferno/GamePlayerSwitcher.js";
import GameToggle from "./view/inferno/GameToggle.js";
import {UIKeys} from "./constants/ui-keys";
import GamePlayerGenerator from "./view/inferno/GamePlayerGenerator.js";
import GameOptionsFrame from "./view/inferno/GameOptionsFrame";

/*
Factory used to create Inferno UI elements
 */
export class InfernoUIFactory {

    getPlayerGenerator (upFunc, addDisabled=true, removeDisabled=true) {
        return <GamePlayerGenerator clickFunc={upFunc} addDisabled={addDisabled} removeDisabled={removeDisabled} />;
    }
    getPlayerSwitcher (upFunc, disabled=true) {
        return <GamePlayerSwitcher clickFunc={upFunc} disabled={disabled} />
    }
    getOptionsFrame (upFunc, gridToggleChecked=false, alignToggleChecked=false, disabled=true) {
        return <GameOptionsFrame clickFunc={upFunc} gridToggleChecked={gridToggleChecked} alignToggleChecked={alignToggleChecked} disabled={disabled} />
    }
    getJoystick (downFunc, upFunc, disabled=true) {
        return <GameJoystick disabled={disabled} downFunc={downFunc} upFunc={upFunc} />;
    }
    getGridToggleButton (key, label, upFunc, checked=false, disabled=false) {
        return <GameToggle gameKey={key} label={label} clickFunc={upFunc} checked={checked} disabled={disabled} />
    }
    getGeneratorButton (key, upFunc, disabled=false) {
        const cssClass = 'generator';
        let cssIconClass = null;
        switch (key) {
            case UIKeys.ADD_UNIT:
                cssIconClass = 'fas fa-plus';
                break;
            case UIKeys.REMOVE_UNIT:
                cssIconClass = 'fas fa-minus';
                break;
        }

        return <GameButton gameKey={key} buttonClass={cssClass} iconId={null} iconClass={cssIconClass} upFunc={upFunc} disabled={disabled} />
    }
    getSwitcherButton (key, upFunc, disabled=false) {
        const cssClass = 'switcher';
        const cssIconClass = 'fas fa-backward fa-1x';
        let cssIconId = 'arrow-left';
        switch (key) {
            case UIKeys.NEXT_UNIT:
                cssIconId = 'arrow-right';
                break;
        }

        return <GameButton gameKey={key} buttonClass={cssClass} iconId={cssIconId} iconClass={cssIconClass} upFunc={upFunc} disabled={disabled} />
    }
    getJoystickButton (direction, downFunc, upFunc, leaveFunc, disabled=false) {
        const cssClass = 'direction';
        const cssIconClass = 'fas fa-arrow-left fa-2x';
        let cssIconId = 'arrow-left';
        switch (direction) {
            case WalkDirections.SOUTH:
                cssIconId = 'arrow-bottom';
                break;
            case WalkDirections.NORTH:
                cssIconId = 'arrow-top';
                break;
            case WalkDirections.WEST:
                cssIconId = 'arrow-left';
                break;
            case WalkDirections.EAST:
                cssIconId = 'arrow-right';
                break;
            case WalkDirections.NORTH_EAST:
                cssIconId = 'arrow-top-right';
                break;
            case WalkDirections.NORTH_WEST:
                cssIconId = 'arrow-top-left';
                break;
            case WalkDirections.SOUTH_EAST:
                cssIconId = 'arrow-bottom-right';
                break;
            case WalkDirections.SOUTH_WEST:
                cssIconId = 'arrow-bottom-left';
                break;
        }

        return <GameButton gameKey={direction} buttonClass={cssClass} iconId={cssIconId} iconClass={cssIconClass} leaveFunc={leaveFunc} downFunc={downFunc} upFunc={upFunc} disabled={disabled} />
    }
}
