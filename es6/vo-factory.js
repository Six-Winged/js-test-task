/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/26/18 8:54 PM
 */

import {GridVO} from "./model/grid-vo.js";
import {GridPointVO} from "./model/grid-point-vo.js";
import {UnitVO} from "./model/unit-vo.js";
import {UnitTypes} from "./constants/unit-types.js";
import {UnitWalkData} from "./model/unit-walk-data.js";

/*
Factory used to created game value objects
 */
export class VOFactory {

    getUnitVO ({type=UnitTypes.REGULAR_UNIT, id='unit'+Math.round (Math.random() * 999999), config}) {
        if (!config) {
            throw new Error ('VOFactory "getUnitVO" error! Config required.');
        }
        return new UnitVO (type, id, config);
    }

    getGridVO ({columns=10, rows=10, startColumn=0, startRow=0}) {
        return new GridVO (columns, rows, startColumn, startRow);
    }

    getGridPoint ({column=-1, row=-1}) {
        return new GridPointVO (column, row);
    }

    getWalkingData ({targetColumn=-1, targetRow=-1, middlePoint=null, direction=null}) {
        return new UnitWalkData (targetColumn, targetRow, middlePoint, direction);
    }

}
