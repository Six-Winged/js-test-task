/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/29/18 10:48 AM
 */

import {WalkDirections} from "../constants/walk-directions.js";
import {UIKeys} from "../constants/ui-keys.js";

/*
Expected controller as element of MVC model
 */
export class LocationController {

    constructor (model) {
        this._model = model;
        this._keysUsed = [];
    }

    hudJoystickDown (direction) {
        this._keysUsed.length = 0;
        (direction) ? this._walkRequest (direction) : this._walkRequest (WalkDirections.STOPS);
    }
    hudSwitcherUp (key) {
        (key === UIKeys.NEXT_UNIT) ? this._model.switchUnit() : this._model.switchUnit(false);
    }
    hudGeneratorUp (key) {
        (key === UIKeys.ADD_UNIT) ? this._model.addRandomUnit() : this._model.removeRandomUnit();
    }
    hudOptionsUp (key, request) {
        (key === UIKeys.SHOW_HIDE_GRID) ? this._model.setDrawGrid(request) : this._model.setGridAlign(request);
    }

    locationKeyDown (keyCode) {
        if (!this._model.getInputEnabled ()) return;

        const naviCodes = [37, 38, 39, 40];
        if (naviCodes.indexOf (keyCode) !== -1) {
            if (this._keysUsed.indexOf (keyCode) === -1) this._keysUsed.push (keyCode);
            if (this._keysUsed.length > 2) this._keysUsed.splice (2, this._keysUsed.length-1);

            this._detectKeyDirection ();
            // console.log (`key down ${this._keysUsed.length} ${this._keysUsed}`);
            // console.log (`key down ${this._keysUsed [0]} ${direction}`);
        }
    }
    locationKeyUp (keyCode) {
        if (!this._model.getInputEnabled ()) return;

        const naviCodes = [37, 38, 39, 40];
        if (naviCodes.indexOf (keyCode) !== -1) {
            const kindex = this._keysUsed.indexOf (keyCode);
            if (kindex !== -1) this._keysUsed.splice (kindex, 1);
            this._detectKeyDirection ();
            // console.log (`key up ${this._keysUsed.length} ${this._keysUsed}`);
        } else if (keyCode === 32) {
            this._model.switchUnit();
            this._detectKeyDirection ();
        }
    }
    _walkRequest (direction) {
        // console.log (`LocationController walkRequest: ${direction}`);
        this._model.walkSelectedUnit (direction);
    }
    _detectKeyDirection () {
        if (this._keysUsed.length === 0) {
            // this._controller.walkRequest (null);
            this._walkRequest (WalkDirections.STOPS);
            return;
        }

        let direction;
        if (this._keysUsed.length === 1) {
            switch (this._keysUsed [0]) {
                case 37: direction = WalkDirections.WEST; break;
                case 38: direction = WalkDirections.NORTH; break;
                case 39: direction = WalkDirections.EAST; break;
                case 40: direction = WalkDirections.SOUTH; break;
            }
        } else {
            const leftBool = this._keysUsed.indexOf (37) !== -1;
            const topBool = this._keysUsed.indexOf (38) !== -1;
            const rightBool = this._keysUsed.indexOf (39) !== -1;
            const bottomBool = this._keysUsed.indexOf (40) !== -1;
            if (leftBool && topBool) direction = WalkDirections.NORTH_WEST;
            else if (leftBool && bottomBool) direction = WalkDirections.SOUTH_WEST;
            else if (rightBool && bottomBool) direction = WalkDirections.SOUTH_EAST;
            else if (rightBool && topBool) direction = WalkDirections.NORTH_EAST;
            else if (this._keysUsed [1] === 37) direction = WalkDirections.EAST;
            else if (this._keysUsed [1] === 39) direction = WalkDirections.WEST;
            else if (this._keysUsed [1] === 38) direction = WalkDirections.NORTH;
            else if (this._keysUsed [1] === 40) direction = WalkDirections.SOUTH;
        }
        this._walkRequest (direction);
    }
}