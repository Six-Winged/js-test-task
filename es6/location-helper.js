/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/28/18 10:32 PM
 */

import {WalkDirections} from "./constants/walk-directions.js";

/*
Helper to resolve some specific location calculations
 */
export class LocationHelper {

    static getPointsDistance (point1, point2) {
        const DX = point1.x - point2.x;
        const DY = point1.y - point2.y;
        return Math.sqrt (Math.pow (DX, 2) + Math.pow (DY, 2));
    }

    static getOppositeDirection (direction) {
        switch (direction) {
            case WalkDirections.EAST:
                return WalkDirections.WEST;
            case WalkDirections.WEST:
                return WalkDirections.EAST;
            case WalkDirections.NORTH:
                return WalkDirections.SOUTH;
            case WalkDirections.SOUTH:
                return WalkDirections.NORTH;

            case WalkDirections.SOUTH_EAST:
                return WalkDirections.NORTH_WEST;
            case WalkDirections.SOUTH_WEST:
                return WalkDirections.NORTH_EAST;
            case WalkDirections.NORTH_WEST:
                return WalkDirections.SOUTH_EAST;
            case WalkDirections.NORTH_EAST:
                return WalkDirections.SOUTH_WEST;

            default:
                return direction;
        }
    }
    static areDirectionsOpposite (requested, current) {
        switch (requested) {
            case WalkDirections.EAST:
                return current === WalkDirections.WEST;
            case WalkDirections.WEST:
                return current === WalkDirections.EAST;
            case WalkDirections.NORTH:
                return current === WalkDirections.SOUTH;
            case WalkDirections.SOUTH:
                return current === WalkDirections.NORTH;

            case WalkDirections.SOUTH_EAST:
                return current === WalkDirections.NORTH_WEST;
            case WalkDirections.SOUTH_WEST:
                return current === WalkDirections.NORTH_EAST;
            case WalkDirections.NORTH_WEST:
                return current === WalkDirections.SOUTH_EAST;
            case WalkDirections.NORTH_EAST:
                return current === WalkDirections.SOUTH_WEST;

            default:
                return false;
        }
    }
    static areDirectionsSimilar (requested, current) {
        switch (requested) {
            case WalkDirections.EAST:
                return current === WalkDirections.SOUTH_EAST || current === WalkDirections.NORTH_EAST;
            case WalkDirections.WEST:
                return current === WalkDirections.SOUTH_WEST || current === WalkDirections.NORTH_WEST;
            case WalkDirections.NORTH:
                return current === WalkDirections.NORTH_WEST || current === WalkDirections.NORTH_EAST;
            case WalkDirections.SOUTH:
                return current === WalkDirections.SOUTH_WEST || current === WalkDirections.SOUTH_EAST;

            case WalkDirections.SOUTH_EAST:
                return current === WalkDirections.SOUTH || current === WalkDirections.EAST;
            case WalkDirections.SOUTH_WEST:
                return current === WalkDirections.SOUTH || current === WalkDirections.WEST;
            case WalkDirections.NORTH_WEST:
                return current === WalkDirections.NORTH || current === WalkDirections.WEST;
            case WalkDirections.NORTH_EAST:
                return current === WalkDirections.NORTH || current === WalkDirections.EAST;

            default:
                return false;
        }
    }

    static getCartesianCoordinates (column, row, tileWidth, tileHeight) {
        const x = tileWidth / 2 * column + tileWidth / 2 * row + tileWidth / 2;
        const y = - tileHeight / 2 * column + tileHeight / 2 * row;
        return new PIXI.Point (x, y);
    }

    static getMiddlePoint (point0, point1, point2, distance) {
        const fullDistance = LocationHelper.getPointsDistance (point0, point2);
        const totalDistance = LocationHelper.getPointsDistance (point1, point2);
        if (totalDistance < distance) {
            return {point:null, proportion:0};
        } else if (totalDistance === distance) {
            return {point:point2.clone (), proportion:1};
        } else {
            const ddistance = distance / totalDistance;

            // method #1
            // const resX = (1 - ddistance) * point1.x + ddistance * point2.x;
            // const resY = (1 - ddistance) * point1.y + ddistance * point2.y;

            // method #2
            const resX = point1.x + ddistance * (point2.x - point1.x);
            const resY = point1.y + ddistance * (point2.y - point1.y);

            return {point:new PIXI.Point (resX, resY), proportion:(fullDistance-totalDistance)/fullDistance};
        }
    }

}
