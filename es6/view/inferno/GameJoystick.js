import {Component} from 'inferno';
import {InfernoUIFactory} from "../../inferno-ui-factory.js";
import {WalkDirections} from "../../constants/walk-directions.js";

class GameJoystick extends Component {
    _navMouseDownHandler (gameKey) {
        this.props.downFunc (gameKey);
    }
    _navMouseUpHandler (gameKey) {
        this.props.upFunc (gameKey);
    }
    componentDidMount () {
        this.div.style.opacity = 0;
        TweenLite.to (this.div.style, .5, {opacity:1}).delay (.5);
    }
    render () {
        const uiFactory = new InfernoUIFactory;
        const downFunc = this._navMouseDownHandler.bind (this);
        const upFunc = this._navMouseUpHandler.bind (this);
        const leaveFunc = this._navMouseUpHandler.bind (this);
        const disabled = this.props.disabled;

        return  <div ref={div => this.div = div} className="joystick-container">
                    {uiFactory.getJoystickButton (WalkDirections.NORTH_WEST, downFunc, upFunc, leaveFunc, disabled)}
                    {uiFactory.getJoystickButton (WalkDirections.NORTH, downFunc, upFunc, leaveFunc, disabled)}
                    {uiFactory.getJoystickButton (WalkDirections.NORTH_EAST, downFunc, upFunc, leaveFunc, disabled)}
                    {uiFactory.getJoystickButton (WalkDirections.WEST, downFunc, upFunc, leaveFunc, disabled)}
                    <div></div>
                    {uiFactory.getJoystickButton (WalkDirections.EAST, downFunc, upFunc, leaveFunc, disabled)}
                    {uiFactory.getJoystickButton (WalkDirections.SOUTH_WEST, downFunc, upFunc, leaveFunc, disabled)}
                    {uiFactory.getJoystickButton (WalkDirections.SOUTH, downFunc, upFunc, leaveFunc, disabled)}
                    {uiFactory.getJoystickButton (WalkDirections.SOUTH_EAST, downFunc, upFunc, leaveFunc, disabled)}
                </div>;
    }
}
GameJoystick.defaultProps = {
    disabled: true,
    downFunc: ()=>{},
    upFunc: ()=>{},
};

export default GameJoystick;
