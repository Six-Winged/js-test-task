import {Component} from 'inferno';

class GameButton extends Component {
    constructor (props) {
        super (props);
        this._selected = false;
        this._gameKey = this.props.gameKey;
    }

    _mouseDownHandler () {
        if (this.props.disabled) return;
        this._selected = true;
        this.props.downFunc (this._gameKey);
    }
    _mouseUpHandler () {
        if (!this._selected) return;
        this._selected = false;
        if (this.props.disabled) return;
        this.props.upFunc (this._gameKey);
    }
    _mouseLeaveHandler () {
        if (!this._selected) return;
        this._selected = false;
        if (this.props.disabled) return;
        this.props.leaveFunc (this._gameKey);
    }
    render () {
        const {buttonClass, iconId, iconClass, label='', disabled} = this.props;
        return <button disabled={disabled}
                    onMouseDown={this._mouseDownHandler.bind (this)}
                    onMouseUp={this._mouseUpHandler.bind (this)}
                    onMouseLeave={this._mouseLeaveHandler.bind (this)}
                    className={buttonClass}>
                        <i id={iconId} className={iconClass}></i> {label}
                </button>
    }
}
GameButton.defaultProps = {
    disabled: true,
    downFunc: ()=>{},
    upFunc: ()=>{},
    clickFunc: ()=>{},
    leaveFunc: ()=>{},
};

export default GameButton;
