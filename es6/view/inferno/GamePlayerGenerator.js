import {Component} from 'inferno';
import {InfernoUIFactory} from "../../inferno-ui-factory.js";
import {UIKeys} from "../../constants/ui-keys.js";

class GamePlayerGenerator extends Component {
    _navMouseClickHandler (gameKey) {
        this.props.clickFunc (gameKey);
    }
    componentDidMount () {
        this.div.style.opacity = 0;
        TweenLite.to (this.div.style, .5, {opacity:1}).delay (.5);
    }
    render () {
        const uiFactory = new InfernoUIFactory;
        const clickFunc = this._navMouseClickHandler.bind (this);

        return  <div ref={div => this.div = div} className="generator-container">
                    {uiFactory.getGeneratorButton (UIKeys.REMOVE_UNIT, clickFunc, this.props.removeDisabled)}
                    {uiFactory.getGeneratorButton (UIKeys.ADD_UNIT, clickFunc, this.props.addDisabled)}
                </div>;
    }
}
GamePlayerGenerator.defaultProps = {
    addDisabled: true,
    removeDisabled: true,
    clickFunc: ()=>{},
};

export default GamePlayerGenerator;
