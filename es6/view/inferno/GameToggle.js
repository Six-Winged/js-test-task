/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 9/6/18 10:16 AM
 */
import {Component} from 'inferno';

class GameToggle extends Component {

    _mouseUpHandler () {
        if (this.props.disabled) return;
        this.props.clickFunc (this.props.gameKey, !this.checkbox.checked);
    }
    render () {
        return  <div className={this.props.containerClass}>
                    <label className="switch">
                            <input
                                type="checkbox"
                                disabled={this.props.disabled}
                                checked={this.props.checked}
                                ref={checkbox => this.checkbox = checkbox}/>
                            <span
                                className="slider round"
                                onMouseUp={this._mouseUpHandler.bind(this)} />
                    </label>
                    {this.props.label}
                </div>;
    }
}
GameToggle.defaultProps = {
    disabled: false,
    checked: true,
    containerClass: 'switch-container',
    label: 'No title',
    gameKey: null,
    clickFunc: ()=>{},
};

export default GameToggle;
