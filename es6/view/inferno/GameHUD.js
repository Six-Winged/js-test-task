import {Component} from 'inferno';
import {InfernoUIFactory} from "../../inferno-ui-factory.js";
import {AppEvent} from "../../constants/app-event";
import {UIKeys} from "../../constants/ui-keys";
import {WalkDirections} from "../../constants/walk-directions";

class GameHUD extends Component {

    constructor (props) {
        super (props);

        this.props.model.addListener (AppEvent.GAME_LAUNCHED, (eventType) => this._modelHandler (eventType));
        this.props.model.addListener (AppEvent.UNIT_ADDED, (eventType) => this._modelHandler (eventType));
        this.props.model.addListener (AppEvent.UNIT_REMOVED, (eventType) => this._modelHandler (eventType));
        this.props.model.addListener (AppEvent.CONFIG_UPDATED, (eventType) => this._modelHandler (eventType));
        this.state = {joystickDisabled:true,
                      switcherDisabled:true,
                      generatorAddDisabled:true,
                      generatorRemoveDisabled:true,
                      gridToggleChecked:false,
                      alignToggleChecked:false,
                      optionsDisabled:true};
    }

    _modelHandler (eventType) {
        switch (eventType) {
            case AppEvent.CONFIG_UPDATED:
                this.setState ({gridToggleChecked: this.props.model.getConfig ().drawGrids,
                                alignToggleChecked: this.props.model.getConfig ().gridAlign});
                break;
            case AppEvent.GAME_LAUNCHED:
            case AppEvent.UNIT_ADDED:
            case AppEvent.UNIT_REMOVED:
                const unitsNum = this.props.model.getUnitsNum ();
                this.setState ({joystickDisabled: unitsNum === 0,
                                switcherDisabled: unitsNum <= 1,
                                generatorRemoveDisabled: unitsNum === 0,
                                generatorAddDisabled: unitsNum >= this.props.gameConfig.maxUnits,
                                gridToggleChecked: this.props.model.getConfig ().drawGrids,
                                alignToggleChecked: this.props.model.getConfig ().gridAlign,
                                optionsDisabled:false});
                break;
        }
    }

    _joystickDownHandler (key) {
        // console.log (`_joystickDownHandler: ${key}`);
        this._hudDownHandler (key);
    }
    _joystickUpHandler (key) {
        // console.log (`_joystickUpHandler: ${key}`);
        this._hudUpHandler (key);
    }
    _switcherUpHandler (key) {
        // console.log (`_switcherUpHandler: ${key}`);
        this._hudUpHandler (key);
    }
    _generatorUpHandler (key) {
        // console.log (`_generatorUpHandler: ${key}`);
        this._hudUpHandler (key);
    }
    _optionsHandler (key, request) {
        // console.log (`_optionsHandler: ${key} ${request}`);
        this._hudUpHandler (key, request);
    }
    render () {
        const uiFactory = new InfernoUIFactory;
        return  <div>
                    {uiFactory.getOptionsFrame (this._optionsHandler.bind (this), this.state.gridToggleChecked, this.state.alignToggleChecked, this.state.optionsDisabled)}
                    {uiFactory.getPlayerGenerator (this._generatorUpHandler.bind (this), this.state.generatorAddDisabled, this.state.generatorRemoveDisabled)}
                    {uiFactory.getJoystick (this._joystickDownHandler.bind (this), this._joystickUpHandler.bind (this), this.state.joystickDisabled)}
                    {uiFactory.getPlayerSwitcher (this._switcherUpHandler.bind (this), this.state.switcherDisabled)}
                </div>;
    }


    _hudDownHandler (key) {
        switch (key) {
            case WalkDirections.SOUTH_EAST:
            case WalkDirections.SOUTH:
            case WalkDirections.SOUTH_WEST:
            case WalkDirections.EAST:
            case WalkDirections.NORTH_WEST:
            case WalkDirections.NORTH_EAST:
            case WalkDirections.WEST:
            case WalkDirections.NORTH:
                this.props.controller.hudJoystickDown (key);
                break;
        }
    }
    _hudUpHandler (key, misc) {
        switch (key) {
            case WalkDirections.SOUTH_EAST:
            case WalkDirections.SOUTH:
            case WalkDirections.SOUTH_WEST:
            case WalkDirections.EAST:
            case WalkDirections.NORTH_WEST:
            case WalkDirections.NORTH_EAST:
            case WalkDirections.WEST:
            case WalkDirections.NORTH:
                this.props.controller.hudJoystickDown (null);
                break;

            case UIKeys.NEXT_UNIT:
            case UIKeys.PREV_UNIT:
                this.props.controller.hudSwitcherUp (key);
                break;

            case UIKeys.ADD_UNIT:
            case UIKeys.REMOVE_UNIT:
                this.props.controller.hudGeneratorUp (key);
                break;

            case UIKeys.SHOW_HIDE_GRID:
            case UIKeys.UNIT_ALIGN:
                this.props.controller.hudOptionsUp (key, misc);
                break;
        }
    }

}

GameHUD.defaultProps = {
    downFunc: ()=>{},
    upFunc: ()=>{},
    model: {addListener: ()=>{}, getUnitsNum: ()=> {return 10}},
    controller: {hudJoystickDown: ()=>{}, hudSwitcherUp: ()=>{}, hudGeneratorUp: ()=>{}, hudOptionsUp: ()=>{}},
    gameConfig: {},
};

export default GameHUD;
