import {Component} from 'inferno';
import {InfernoUIFactory} from "../../inferno-ui-factory.js";
import {UIKeys} from "../../constants/ui-keys";

class GamePlayerSwitcher extends Component {
    _navMouseClickHandler (gameKey) {
        this.props.clickFunc (gameKey);
    }
    componentDidMount () {
        this.div.style.opacity = 0;
        TweenLite.to (this.div.style, .5, {opacity:1}).delay (.5);
    }
    render () {
        const uiFactory = new InfernoUIFactory;
        const clickFunc = this._navMouseClickHandler.bind (this);
        const disabled = this.props.disabled;

        return  <div ref={div => this.div = div} className="switcher-container">
                    {uiFactory.getSwitcherButton (UIKeys.PREV_UNIT, clickFunc, disabled)}
                    {uiFactory.getSwitcherButton (UIKeys.NEXT_UNIT, clickFunc, disabled)}
                </div>;
    }
}
GamePlayerSwitcher.defaultProps = {
    disabled: true,
    clickFunc: ()=>{},
};

export default GamePlayerSwitcher;
