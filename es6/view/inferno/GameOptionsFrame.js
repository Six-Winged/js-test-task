/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 9/6/18 12:08 PM
 */

import {Component} from 'inferno';
import {InfernoUIFactory} from "../../inferno-ui-factory.js";
import {UIKeys} from "../../constants/ui-keys.js";

class GameOptionsFrame extends Component {
    _clickHandler (gameKey, request) {
        this.props.clickFunc (gameKey, request);
    }
    componentDidMount () {
        this.div.style.opacity = 0;
        TweenLite.to (this.div.style, .5, {opacity:1}).delay (.5);
    }
    render () {
        const uiFactory = new InfernoUIFactory;
        return  <div ref={div => this.div = div} className="options-container">
                    {uiFactory.getGridToggleButton (UIKeys.SHOW_HIDE_GRID, 'Grid', this._clickHandler.bind (this), this.props.gridToggleChecked, this.props.disabled)}
                    {uiFactory.getGridToggleButton (UIKeys.UNIT_ALIGN, 'Unit align', this._clickHandler.bind (this), this.props.alignToggleChecked, this.props.disabled)}
                </div>;
    }
}
GameOptionsFrame.defaultProps = {
    disabled: false,
    gridToggleChecked: false,
    alignToggleChecked: false,
    clickFunc: ()=>{},
};

export default GameOptionsFrame;
