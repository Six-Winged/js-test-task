/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/28/18 8:50 PM
 */

import {UIFactory} from "../ui-factory.js";
import {UIKeys} from "../constants/ui-keys.js";
import {AppEvent} from "../constants/app-event.js";
import {LocationHelper} from "../location-helper.js";
import {UnitTypes} from "../constants/unit-types.js";

/*
Location visual component (as one of MVC elements)
 */
export class LocationView extends PIXI.Container {

    constructor (model, controller, config, ticker) {
        super ();

        this._model = model;
        this._controller = controller;
        this._config = config;
        this._ticker = ticker;
        this._launched = false;
        this._gridsDrawn = false;
        this._model.addListener (AppEvent.GAME_LAUNCHED, (eventType) => this._modelHandler (eventType));
        this._model.addListener (AppEvent.UNITS_MOVE, (eventType) => this._modelHandler (eventType));
        this._model.addListener (AppEvent.UNITS_SELECTED, (eventType) => this._modelHandler (eventType));
        this._model.addListener (AppEvent.RESIZED, (eventType) => this._modelHandler (eventType));
        this._model.addListener (AppEvent.UNIT_ADDED, (eventType, unitID) => this._modelHandler (eventType, unitID));
        this._model.addListener (AppEvent.UNIT_REMOVED, (eventType, unitID) => this._modelHandler (eventType, unitID));
        this._model.addListener (AppEvent.CONFIG_UPDATED, (eventType, unitID) => this._modelHandler (eventType, unitID));

        this._uiFactory = new UIFactory;

        this._currentUnitSelection = null;
        this._unitSelectionsHashTable = {};

        this._mainHolder = new PIXI.Container;
        this._backHolder = new PIXI.Container;
        this._gridsHolder = new PIXI.Graphics;
        this._selectionsHolder = new PIXI.Container;
        this._unitsHolder = new PIXI.Container;
        this._viewArea = new PIXI.Graphics;
        this.addChild (this._mainHolder);
        this.addChild (this._viewArea);
        this._mainHolder.addChild (this._backHolder);
        this._mainHolder.addChild (this._gridsHolder);
        this._mainHolder.addChild (this._selectionsHolder);
        this._mainHolder.addChild (this._unitsHolder);
        this._mainHolder.visible = false;

        this._mainPosition = new PIXI.Point (0, 0);
        this._gridsStartPoint = new PIXI.Point (0, 0);
        this._back = null;
        this._grids = [];
        this._units = [];
        this._unitsHashTable = {};
        this._bounds = new PIXI.Rectangle (0, 0, 100, 100);
    }

    _modelHandler (eventType, args) {
        switch (eventType) {
            case AppEvent.GAME_LAUNCHED:
                this._launch ();
                break;
            case AppEvent.UNITS_MOVE:
                this._updateUnits ();
                break;
            case AppEvent.RESIZED:
                this._setBounds (this._model.getScreenArea (), this._model.getViewArea ());
                break;
            case AppEvent.UNITS_SELECTED:
                this._checkUnitVisibility (this._getSelectedUnit (), true, false);
                break;
            case AppEvent.UNIT_ADDED:
                this._addUnit (this._model.getUnitByID (args));
                break;
            case AppEvent.UNIT_REMOVED:
                this._removeUnit (args);
                break;
            case AppEvent.CONFIG_UPDATED:
                this._gridsHolder.visible = this._config.drawGrids;
                if (!this._gridsDrawn && this._config.drawGrids) {
                    const gridsIterator = this._model.getGrids();
                    let gridsIteratorItem = gridsIterator.next();
                    while (!gridsIteratorItem.done) {
                        this._drawGrid (gridsIteratorItem.value);
                        gridsIteratorItem = gridsIterator.next();
                    }
                    this._gridsDrawn = true;
                }
                break;
        }
    }

    _launch () {
        if (this._launched) return;
        this._launched = true;

        // this._tileBounds = this._model.getTileBounds ();
        this._gridsStartPoint = this._model.getGridsStartPoint();
        this._setBackground (this._model.getLocationID ());
        this._setBounds (this._model.getScreenArea (), this._model.getViewArea ());
        this._setRandomPosition ();

        this._grids.length = 0;
        const gridsIterator = this._model.getGrids();
        let gridsIteratorItem = gridsIterator.next();
        while (!gridsIteratorItem.done) {
            this._addGrid (gridsIteratorItem.value, this._config.drawGrids);
            gridsIteratorItem = gridsIterator.next();
        }
        this._gridsDrawn = this._config.drawGrids;

        this._clearUnits ();
        const unitsIterator = this._model.getUnits();
        let unitsIteratorItem = unitsIterator.next();
        while (!unitsIteratorItem.done) {
            this._addUnit (unitsIteratorItem.value);
            unitsIteratorItem = unitsIterator.next();
        }

        this._addUnitSelection (UIKeys.UNIT_SELECTION_WHITE);
        this._addUnitSelection (UIKeys.UNIT_SELECTION_YELLOW);
        this._addUnitSelection (UIKeys.UNIT_SELECTION_BLUE);
        this._addUnitSelection (UIKeys.UNIT_SELECTION_ZOMBIE);

        this._ticker.add (this._pixiLoop, this);

        // if (this._model.getInputEnabled ()) this._enableInput ();
        // this._checkUnitVisibility (this._units [0], true, false);

        const appearTime = (this._config.drawGrids) ? .2 : .3;
        this._mainHolder.visible = true;
        this._mainHolder.alpha = 0;
        TweenLite.to (this._mainHolder, appearTime, {alpha:1, ease:Power0.easeNone, onComplete:() => this._appeared (), onCompleteParams:[this]});
    }

    _pixiLoop (delta) {
        if (this._units.length <= 1) {
            if (this._currentUnitSelection && this._currentUnitSelection.visible) {
                this._currentUnitSelection = null;
                this._hideUnitSelections ();
            }
            return; // don't need z-sorting or unit selection for 1 unit
        }

        const selectedUnit = this._getSelectedUnit ();
        if (selectedUnit) {
            let selectionType = null;
            switch (selectedUnit.getType()) {
                case UnitTypes.REGULAR_UNIT:
                    selectionType = UIKeys.UNIT_SELECTION_WHITE;
                    break;
                case UnitTypes.FAST_UNIT:
                    selectionType = UIKeys.UNIT_SELECTION_YELLOW;
                    break;
                case UnitTypes.SLOW_UNIT:
                    selectionType = UIKeys.UNIT_SELECTION_BLUE;
                    break;
                case UnitTypes.TOO_SLOW_UNIT:
                    selectionType = UIKeys.UNIT_SELECTION_ZOMBIE;
                    break;
                default:
                    selectionType = UIKeys.UNIT_SELECTION_WHITE;
            }
            const unitSelection = this._getUnitSelection (selectionType);
            if (unitSelection && !unitSelection.visible) {
                this._hideUnitSelections ();
                unitSelection.visible = true;
                unitSelection.play ();
                this._currentUnitSelection = unitSelection;
            }
            unitSelection.x = selectedUnit.x - 17;
            unitSelection.y = selectedUnit.y - 9;
        } else {
            this._currentUnitSelection = null;
            this._hideUnitSelections ();
        }

        // z-sorting
        this._units.sort (sortUnits);
        let u = -1, unit;
        while (++u < this._units.length) {
            unit = this._units [u];
            this._unitsHolder.addChild (unit);
        }

    }
    _appeared () {
        if (this._model.getInputEnabled ()) this._enableInput ();
        this._checkUnitVisibility (this._getSelectedUnit (), true, false);
    }
    _updateUnits () {
        const updateIterator = this._model.getUnitsToUpdate ();
        if (!updateIterator) return;
        // console.log (`update units: ${updateIterator}`);

        let updatedUnitData = updateIterator.next (), updatedUnit, updatedUnitMovie;
        while (!updatedUnitData.done) {
            updatedUnit = updatedUnitData.value;
            updatedUnitMovie = this._unitsHashTable [updatedUnit.getID ()];
            if (updatedUnitMovie) {
                updatedUnitMovie.setState (updatedUnit.getWalkDirection (), true);
                this._updateUnitPosition (updatedUnitMovie, updatedUnit, false);
            }
            updatedUnitData = updateIterator.next ();
        }
    }
    _enableInput () {
        if (!this._back) return;
        this.interactive = true;
        this.on ('mousedown', this._mouseDownHandler).
             on ('touchstart', this._mouseDownHandler);

        window.addEventListener ('keydown', (event) => this._keyDownHandler (event));
        window.addEventListener ('keyup', (event) => this._keyUpHandler (event));
    }
    _setRandomPosition () {
        if (!this._back || !this._bounds) return;

        const holderBounds = this._getHolderBounds ();
        if (this._mainHolder.width > this._bounds.width) {
            this._mainHolder.x = (holderBounds.maxX - holderBounds.minX) * Math.random() + holderBounds.minX;
        }
        if (this._mainHolder.height > this._bounds.height) {
            this._mainHolder.y = (holderBounds.maxY - holderBounds.minY) * Math.random() + holderBounds.minY;
        }
    }
    _getHolderBounds () {
        let minX, maxX, minY, maxY;
        if (this._mainHolder.width > this._bounds.width) {
            minX = this._bounds.x + this._bounds.width - this._mainHolder.width;
            maxX = this._bounds.x;
        } else {
            minX = maxX = (this._bounds.width - this._mainHolder.width) / 2;
        }
        if (this._mainHolder.height > this._bounds.height) {
            minY = this._bounds.y + this._bounds.height - this._mainHolder.height;
            maxY = this._bounds.y;
        } else {
            minY = maxY = (this._bounds.height - this._mainHolder.height) / 2;
        }
        return {minX:minX, maxX:maxX, minY:minY, maxY:maxY};
    }

    _clearUnits () {
        let un = this._units.length;
        while (--un >= 0) {
            this._units [un].destroy ();
        }
        this._units.length = 0;
        this._unitsHashTable = {};
    }
    _getSelectedUnit () {
        const unitVO = this._model.getSelectedUnit ();
        if (!unitVO) return;
        return this._unitsHashTable [unitVO.getID ()];
    }
    _addUnit (unitVO) {
        if (this._unitsHashTable [unitVO.getID ()] !== undefined) {
            console.log (`LocationView warning! Item ${unitVO.getID ()} already added...`);
            return;
        } else if (!unitVO) {
            console.log (`LocationView warning! Can't add undefined unit...`);
            return;
        }

        const unit = this._uiFactory.getUnit (UIKeys.WALKING_WOMAN, unitVO.getType(), unitVO.getID ());
        this._units.push (unit);
        this._unitsHashTable [unitVO.getID ()] = unit;

        this._updateUnitPosition (unit, unitVO);
        this._unitsHolder.addChild (unit);
    }
    _removeUnit (unitID) {
        const unit = this._unitsHashTable [unitID];
        if (!unit) {
            console.log (`LocationView warning! Can't remove undefined unit...`);
            return;
        }

        this._units.splice (this._units.indexOf (unit), 1);
        delete this._unitsHashTable [unitID];
        this._unitsHolder.removeChild (unit);
    }
    _getUnitSelection (key) {
        return this._unitSelectionsHashTable [key];
    }
    _hideUnitSelections () {
        for (const key in this._unitSelectionsHashTable) {
            this._unitSelectionsHashTable [key].stop ();
            this._unitSelectionsHashTable [key].visible = false;
        }
    }
    _addUnitSelection (key) {
        if (this._unitSelectionsHashTable [key]) return;
        const selection = this._uiFactory.getUnitSelection (key);
        selection.visible = false;
        this._unitSelectionsHashTable [key] = selection;
        this._selectionsHolder.addChild (selection);
    }

    _setBounds (bounds, viewBounds) {
        this._bounds = bounds;
        this._correctContent ();

        this._viewArea.clear();
        this._viewArea.beginFill (0x0000ff, 0);
        this._viewArea.drawRect (0, 0, viewBounds.width, viewBounds.height);
        this._viewArea.endFill ();
        this._viewArea.x = (this._bounds.width - this._viewArea.width) / 2;
        this._viewArea.y = (this._bounds.height - this._viewArea.height) / 2;
    }
    _addGrid (gridVO, drawGrid=false) {
        this._grids.push (gridVO);
        if (drawGrid) this._drawGrid (gridVO);
    }
    _checkUnitVisibility (unit, doFix=false, quick=true) {
        if (!unit) return;
        const unitGlobalPosition = unit.getGlobalPosition ();
        const hitUnit = unitGlobalPosition.x >= this._viewArea.x &&
                        unitGlobalPosition.y >= this._viewArea.y &&
                        unitGlobalPosition.x <= (this._viewArea.width) &&
                        unitGlobalPosition.y <= (this._viewArea.y+this._viewArea.height);
        if (!hitUnit && doFix) {
            const viewCenter = new PIXI.Point (this._viewArea.x+this._viewArea.width/2, this._viewArea.y+this._viewArea.height/2);
            const DX = viewCenter.x - unitGlobalPosition.x;
            const DY = viewCenter.y - unitGlobalPosition.y;

            const holderBounds = this._getHolderBounds ();
            let newX = this._mainHolder.x+DX;
            let newY = this._mainHolder.y+DY;
            if (newX < holderBounds.minX) newX = holderBounds.minX;
            else if (newX > holderBounds.maxX) newX = holderBounds.maxX;
            if (newY < holderBounds.minY) newY = holderBounds.minY;
            else if (newY > holderBounds.maxY) newY = holderBounds.maxY;

            if (quick) {
                TweenLite.killTweensOf (this._mainHolder, false, {x:true, y:true});
                this._mainHolder.x = newX;
                this._mainHolder.y = newY;
            } else {
                TweenLite.to (this._mainHolder, 1, {x:newX, y:newY, ease:Power1.easeOut});
            }
        }
    }
    _drawGrid (gridVO) {
        const colors = [0xffff00, 0xfebebf, 0xfffec1, 0xc0fec2, 0xc0fcfe, 0xc1bffd, 0xf0c0fd];
        const color = colors [Math.round (Math.random() * (colors.length-1))];
        this._gridsHolder.beginFill (color, 1);

        const sX = this._gridsStartPoint.x;
        const sY = this._gridsStartPoint.y;

        this._gridsHolder.moveTo (sX, sY);

        const cellsIterator = gridVO.getPoints();
        let cellItem = cellsIterator.next (), cell, cellCoords;
        let dotCenterX, dotCenterY;
        while (!cellItem.done) {
            cell = cellItem.value;
            cellCoords = LocationHelper.getCartesianCoordinates (cell.column, cell.row, this._config.tileWidth, this._config.tileHeight);

            dotCenterX = Math.round (sX + cellCoords.x);
            dotCenterY =  Math.round (sY + cellCoords.y);
            this._gridsHolder.drawEllipse (dotCenterX, dotCenterY, 2, 1);

/*
            const polyBorderX = 5, polyBorderY = 3;
            this._gridsHolder.drawPolygon ([
                sX+cellCoords.x-this._config.tileWidth/2+polyBorderX, sY+cellCoords.y,
                sX+cellCoords.x, sY+cellCoords.y-this._config.tileHeight/2+polyBorderY,
                sX+cellCoords.x+this._config.tileWidth/2-polyBorderX, sY+cellCoords.y,
                sX+cellCoords.x, sY+cellCoords.y+this._config.tileHeight/2-polyBorderY
            ]);
*/
            cellItem = cellsIterator.next ();
        }
        this._gridsHolder.endFill ();
    }
    _setBackground (key) {
        if (this._back) {
            this._backHolder.removeChild (this._back);
            this._back.destroy ();
        }

        this._back = PIXI.Sprite.from (this._uiFactory.getLocationBackground (key));
        this._backHolder.addChild (this._back);
        this._correctContent ();
    }

    _correctContent () {
        if (!this._mainHolder || !this._bounds) return;

        if (this._mainHolder.width < this._bounds.width) {
            this._mainHolder.x = (this._bounds.width - this._mainHolder.width) / 2;
        }
        if (this._mainHolder.height < this._bounds.height) {
            this._mainHolder.y = (this._bounds.height - this._mainHolder.height) / 2;
        }

        this._correctPosition (this._mainHolder.x, this._mainHolder.y);
    }
    _keyDownHandler (event) {
        this._controller.locationKeyDown (event.keyCode);
    }
    _keyUpHandler (event) {
        this._controller.locationKeyUp (event.keyCode);
    }
    _mouseMoveHandler (event) {
        // console.log (`mouse move ${event} ${this._mouseDownData} ${this._mouseDownData.getLocalPosition (this)}`);
        if (!this._model.getInputEnabled ()) return;

        const currentPosition = event.data.getLocalPosition (this);

        const dx = currentPosition.x - this._mouseDownData.x;
        const dy = currentPosition.y - this._mouseDownData.y;

        let newX = this._mainPosition.x + dx;
        let newY = this._mainPosition.y + dy;

        this._correctPosition (newX, newY);
    }
    _correctPosition (cX, cY) {
        if (this._bounds) {
            if (cX > this._bounds.x) cX = this._bounds.x;
            else if (cX < (this._bounds.x + this._bounds.width - this._mainHolder.width)) cX = this._bounds.x + this._bounds.width - this._mainHolder.width;
            if (cY > this._bounds.y) cY = this._bounds.y;
            else if (cY < (this._bounds.y + this._bounds.height - this._mainHolder.height)) cY = this._bounds.y + this._bounds.height - this._mainHolder.height;
        }
        TweenLite.killTweensOf (this._mainHolder, false, {x:true, y:true});
        this._mainHolder.x = cX;
        this._mainHolder.y = cY;
    }
    _mouseUpHandler (event) {
        // console.log (`mouse up ${event}`);
        if (!this._model.getInputEnabled ()) return;

        this.off ('mouseupoutside', this._mouseUpHandler).
             off ('mouseup', this._mouseUpHandler).
             off ('mouseupoutside', this._mouseUpHandler).
             off ('touchend', this._mouseUpHandler).
             off ('touchendoutside', this._mouseUpHandler).
             off ('touchmove', this._mouseMoveHandler).
             off ('mousemove', this._mouseMoveHandler);

        // this._checkUnitVisibility (this._getSelectedUnit (), true, false);
    }
    _mouseDownHandler (event) {
        // console.log (`mouse down ${event}`);
        if (!this._model.getInputEnabled ()) return;

        TweenLite.killTweensOf (this._mainHolder, false, {x:true, y:true});
        this._mouseDownData = event.data.getLocalPosition (this);
        this._mainPosition = new PIXI.Point (this._mainHolder.x, this._mainHolder.y);

        this.on ('mouseupoutside', this._mouseUpHandler).
             on ('mouseup', this._mouseUpHandler).
             on ('mouseupoutside', this._mouseUpHandler).
             on ('touchend', this._mouseUpHandler).
             on ('touchendoutside', this._mouseUpHandler).
             on ('touchmove', this._mouseMoveHandler).
             on ('mousemove', this._mouseMoveHandler);
    }
    _updateUnitPosition (unit, unitVO, silent=true) {
        const unitCoords = unitVO.getCoordinates();
        unit.moveTo (this._gridsStartPoint.x + unitCoords.x, this._gridsStartPoint.y + unitCoords.y);
        if (!silent) {
            this._checkUnitVisibility (this._getSelectedUnit (), true, false);
        }
    }

    toString () {
        return `[LocationView]`;
    }
}

function sortUnits (unit1, unit2) {
    if (unit1.y < unit2.y) return -1;
    else if (unit1.y > unit2.y) return 1;
    return 0;
}
