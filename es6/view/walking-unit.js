/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/28/18 8:55 PM
 */

import {WalkDirections} from "../constants/walk-directions.js";

/*
Visual game unit component
 */
export class WalkingUnit extends PIXI.Container {

    constructor (id, type, {northFrames, southFrames, eastFrames, westFrames, northEastFrames, northWestFrames, southEastFrames, southWestFrames}) {
        super ();

        this._id = id;
        this._type = type;
        this._state = null;
        this._currentMovie = null;
        // console.log (`WalkingUnit constructor: ${this._id} ${northFrames.length} ${southFrames.length} ${eastFrames.length} ${westFrames.length} ${northEastFrames.length} ${northWestFrames.length} ${southEastFrames.length} ${southWestFrames.length}`);

        this._north = new PIXI.extras.AnimatedSprite (northFrames);
        this._north.pivot.x = this._north.width * .51;
        this._north.pivot.y = this._north.height * .68;

        this._south = new PIXI.extras.AnimatedSprite (southFrames);
        this._south.pivot.x = this._south.width * .51;
        this._south.pivot.y = this._south.height * .65;

        this._east = new PIXI.extras.AnimatedSprite (eastFrames);
        this._east.pivot.x = this._east.width * .45;
        this._east.pivot.y = this._east.height * .65;

        this._west = new PIXI.extras.AnimatedSprite (westFrames);
        this._west.pivot.x = this._west.width * .55;
        this._west.pivot.y = this._west.height * .65;

        this._northEast = new PIXI.extras.AnimatedSprite (northEastFrames);
        this._northEast.pivot.x = this._northEast.width * .5;
        this._northEast.pivot.y = this._northEast.height * .65;

        this._northWest = new PIXI.extras.AnimatedSprite (northWestFrames);
        this._northWest.pivot.x = this._northWest.width * .55;
        this._northWest.pivot.y = this._northWest.height * .65;

        this._southEast = new PIXI.extras.AnimatedSprite (southEastFrames);
        this._southEast.pivot.x = this._southEast.width * .48;
        this._southEast.pivot.y = this._southEast.height * .65;

        this._southWest = new PIXI.extras.AnimatedSprite (southWestFrames);
        this._southWest.pivot.x = this._southWest.width * .55;
        this._southWest.pivot.y = this._southWest.height * .65;

        // this.setState (WalkDirections.NORTH);
        // this.setState (WalkDirections.SOUTH_EAST);
        const states = [WalkDirections.SOUTH_EAST, WalkDirections.SOUTH_WEST, WalkDirections.NORTH_EAST, WalkDirections.NORTH_WEST, WalkDirections.NORTH, WalkDirections.EAST, WalkDirections.WEST, WalkDirections.SOUTH];
        const randInd = Math.round (Math.random () * (states.length-1));
        this.setState (states [randInd]);
    }

    getID () {
        return this._id;
    }
    getType () {
        return this._type;
    }
    walk () {
        if (this._getCurrentMovie ()) this._getCurrentMovie ().play();
    }
    idle () {
        if (this._getCurrentMovie ()) this._getCurrentMovie ().stop();
    }
    setState (value, doWalk=false) {
        if (value === WalkDirections.STOPS || !value) {
            this.idle ();
            return;
        } else if (value === WalkDirections.FIXING) {
            return;
        }
        const oldMovie = this._getMovieByDirection (this._state);
        const newMovie = this._getMovieByDirection (value);
        let oldMovieFrame = 0;
        const isPlaying = (!oldMovie) ? false : oldMovie.playing;

        if (newMovie && oldMovie === newMovie) {
            if (doWalk && !oldMovie.playing) this.walk ();
            return;
        } else if (oldMovie) {
            oldMovie.stop ();
            oldMovieFrame = oldMovie.currentFrame;
            this.removeChild (oldMovie);
        }
        this._state = value;
        if (newMovie) {
            this._currentMovie = newMovie;
            (!isPlaying) ? newMovie.gotoAndStop (oldMovieFrame) : newMovie.gotoAndPlay (oldMovieFrame);
            this.addChild (newMovie);
        }
        if (doWalk) this.walk ();
    }
    moveTo (mX, mY) {
        this.x = mX;
        this.y = mY;
    }

    _getCurrentMovie () {
        if (this._currentMovie) return this._currentMovie;
        else return this._getMovieByDirection (this._state);
    }
    _getMovieByDirection (value) {
        let movie;
        switch (value) {
            case WalkDirections.EAST:
                movie = this._east;
                break;
            case WalkDirections.WEST:
                movie = this._west;
                break;
            case WalkDirections.SOUTH:
                movie = this._south;
                break;
            case WalkDirections.NORTH:
                movie = this._north;
                break;
            case WalkDirections.NORTH_WEST:
                movie = this._northWest;
                break;
            case WalkDirections.NORTH_EAST:
                movie = this._northEast;
                break;
            case WalkDirections.SOUTH_EAST:
                movie = this._southEast;
                break;
            case WalkDirections.SOUTH_WEST:
                movie = this._southWest;
                break;
        }
        return movie;
    }

    toString () {
        return `[WalkingUnit type='${this._type}' id='${this._id}']`;
    }
}
