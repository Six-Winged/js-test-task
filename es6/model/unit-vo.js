/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/28/18 8:56 PM
 */

import {WalkDirections} from "../constants/walk-directions.js";
import {VOFactory} from "../vo-factory.js";
import {LocationHelper} from "../location-helper.js";

/*
Game unit object, keeps data like 'id', 'type', 'coordinates' and current
movement direction, also calculates field coordinates, used in field view
 */
export class UnitVO {

    constructor (type, id, config) {
        this._type = type;
        this._id = id;

        const voFactory = new VOFactory;
        this._walkData = voFactory.getWalkingData ({});

        this._config = config;
        this._walkDirection = null;
        this._coordinates = new PIXI.Point (0, 0);
    }

    getID () {
        return this._id;
    }
    getType () {
        return this._type;
    }
    getCoordinates () {
        return this._coordinates.clone();
    }

    isMoving () {
        return this.getWalkDirection () !== null;
    }
    getWalkDirection () {
        // return this._walkDirection;
        return (!this._walkData.isEmpty() && // unit walks
                 this._walkDirection !== WalkDirections.STOPS && // and walks to not stop
                 this._walkDirection !== WalkDirections.FIXING && // and to not fix the position
                 this._walkDirection !== null) ?
                        this._walkData.direction :
                        this._walkDirection;
    }
    stopMoving () {
        if (this.isMoving ()) {
            this.setWalkDirection (WalkDirections.STOPS);
        }
    }
    setWalkDirection (direction) {
        this._walkData.resetPendingDirection ();
        this._walkDirection = direction;
    }
    setPosition (column, row) {
        this._walkData.setPosition (column, row);

        if (column >= 0 && row >= 0) {
            const updatedPosition = LocationHelper.getCartesianCoordinates (this._walkData.column, this._walkData.row, this._config.tileWidth, this._config.tileHeight);
            this._coordinates.set (updatedPosition.x, updatedPosition.y);
        }
    }

    updateWalk (searchPointFunction, delta) {
        if (this._walkDirection === null || this._walkData.column < 0 || this._walkData.row < 0) return false;
        // console.log (`update walk: ${this._walkDirection}`);
        // console.log (`update walk: ${searchPointFunction}`);

        let zeroPosition, startPosition, endPosition;
        let searchPointVO;
        let currentSpeed = 0;

        if (this._walkData.isEmpty()) {
            startPosition = LocationHelper.getCartesianCoordinates (this._walkData.column, this._walkData.row, this._config.tileWidth, this._config.tileHeight);
            zeroPosition = startPosition.clone ();
        } else {
            this._walkDirection = this._walkData.checkCurrentCase (this._walkDirection);
            startPosition = this._walkData.middlePoint;
            zeroPosition = LocationHelper.getCartesianCoordinates (this._walkData.column, this._walkData.row, this._config.tileWidth, this._config.tileHeight);
        }

        let currentDirection = this._walkDirection;
        if (currentDirection === WalkDirections.STOPS && !this._config.gridAlign) {
            this._resetWalkData ();
            return true;
        } else if (currentDirection === WalkDirections.STOPS && this._config.gridAlign) {
            this._walkDirection = WalkDirections.FIXING;
            currentDirection = this._walkData.direction;
        } else if (currentDirection === WalkDirections.FIXING) {
            currentDirection = this._walkData.direction;
        }
        switch (currentDirection) {
            case WalkDirections.EAST:
                searchPointVO = searchPointFunction (this._walkData.column+1, this._walkData.row+1);
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column+1, this._walkData.row);
                    if (searchPointVO) currentDirection = WalkDirections.NORTH_EAST;
                }
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column, this._walkData.row+1);
                    if (searchPointVO) currentDirection = WalkDirections.SOUTH_EAST;
                }
                break;
            case WalkDirections.NORTH_EAST:
                searchPointVO = searchPointFunction (this._walkData.column+1, this._walkData.row);
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column+1, this._walkData.row-1);
                    if (searchPointVO) currentDirection = WalkDirections.NORTH;
                }
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column+1, this._walkData.row+1);
                    if (searchPointVO) currentDirection = WalkDirections.EAST;
                }
                break;
            case WalkDirections.SOUTH_EAST:
                searchPointVO = searchPointFunction (this._walkData.column, this._walkData.row+1);
                break;
            case WalkDirections.WEST:
                searchPointVO = searchPointFunction (this._walkData.column-1, this._walkData.row-1);
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column, this._walkData.row-1);
                    if (searchPointVO) currentDirection = WalkDirections.NORTH_WEST;
                }
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column-1, this._walkData.row);
                    if (searchPointVO) currentDirection = WalkDirections.SOUTH_WEST;
                }
                break;
            case WalkDirections.NORTH_WEST:
                searchPointVO = searchPointFunction (this._walkData.column, this._walkData.row-1);
                break;
            case WalkDirections.SOUTH_WEST:
                searchPointVO = searchPointFunction (this._walkData.column-1, this._walkData.row);
                break;
            case WalkDirections.NORTH:
                searchPointVO = searchPointFunction (this._walkData.column+1, this._walkData.row-1);
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column, this._walkData.row-1);
                    if (searchPointVO) currentDirection = WalkDirections.NORTH_WEST;
                }
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column, this._walkData.row+1);
                    if (searchPointVO) currentDirection = WalkDirections.NORTH_EAST;
                }
                break;
            case WalkDirections.SOUTH:
                searchPointVO = searchPointFunction (this._walkData.column-1, this._walkData.row+1);
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column, this._walkData.row+1);
                    if (searchPointVO) currentDirection = WalkDirections.SOUTH_EAST;
                }
                if (!searchPointVO) {
                    searchPointVO = searchPointFunction (this._walkData.column-1, this._walkData.row);
                    if (searchPointVO) currentDirection = WalkDirections.SOUTH_WEST;
                }
                break;
        }

        if (searchPointVO) {
            endPosition = LocationHelper.getCartesianCoordinates (searchPointVO.column, searchPointVO.row, this._config.tileWidth, this._config.tileHeight);
            currentSpeed = this._config.getAnimationSpeed (currentDirection, this._type);
            currentSpeed *= delta;

            // console.log (searchPointVO.column, searchPointVO.row);

            let middlePointObj = LocationHelper.getMiddlePoint (zeroPosition, startPosition, endPosition, currentSpeed);
            let middlePoint = middlePointObj.point;
            // console.log ('middle point proportion:', middlePointObj.proportion);

            if (!middlePoint) {
                // necessary distance more than distance to the target point
                this._coordinates.set (endPosition.x, endPosition.y);
                this._walkData.setPosition (searchPointVO.column, searchPointVO.row);
                this._walkData.reset ();

                if (this._walkDirection === WalkDirections.FIXING) {
                    this._resetWalkData (true);
                    return true;
                } else {
                    // console.log ('...pending direction #1:', this._walkData.pendingDirection);
                    if (this._walkData.pendingDirection) {
                        this._walkDirection = this._walkData.pendingDirection;
                        this._walkData.resetPendingDirection ();
                    }
                    return false;
                }
            } else if (middlePoint.equals (endPosition)) {
                // distance point is what we need
                this._coordinates.set (middlePoint.x, middlePoint.y);
                this._walkData.setPosition (searchPointVO.column, searchPointVO.row);

                if (this._walkDirection === WalkDirections.FIXING) {
                    this._resetWalkData (true);
                    return true;
                } else {
                    this._walkData.reset ();
                    return false;
                }
            } else {
                this._coordinates.set (middlePoint.x, middlePoint.y);
            }
            this._walkData.set (searchPointVO.column, searchPointVO.row, middlePoint, currentDirection, middlePointObj.proportion);
            return true;
        } else if (this._walkDirection === WalkDirections.FIXING) {
            this._resetWalkData (true);
            return true;
        } else {
            return true;
        }
    }

    _resetWalkData (full=false) {
        this._walkDirection = null;
        if (full) this._walkData.reset ();
    }

    toString () {
        return `[UnitVO id="${this._id}" type="${this._type}" column=${this._walkData.column} row=${this._walkData.row}]`;
    }

}
