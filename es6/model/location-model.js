/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/28/18 8:50 PM
 */

import {AppEvent} from "../constants/app-event.js";
import {VOFactory} from "../vo-factory.js";
import {UIKeys} from "../constants/ui-keys.js";
import {UnitTypes} from "../constants/unit-types.js";
import {ScenarioKeys} from "../constants/scenario-keys.js";

/*
Location data holder, element of MVC model
 */
export class LocationModel extends PIXI.utils.EventEmitter {

    constructor (config, appTicker, screenWidth, screenHeight, viewWidth, viewHeight) {
        super ();
        this.launched = false;

        this.config = config;
        this.appTicker = appTicker;
        this.screenArea = new PIXI.Rectangle (0, 0, screenWidth, screenHeight);
        this.viewArea = new PIXI.Rectangle (0, 0, viewWidth, viewHeight);
        this.voFactory = new VOFactory;

        this.grids = [];
        this.units = [];
        this.ticking = false;
        this.unitsHashTable = {};
        this.unitsToUpdate = [];
        this.selectedUnit = null;
        this.locationKey = null;
        this.inputEnabled = false;
    }

    resizeCanvas (screenWidth, screenHeight, viewWidth, viewHeight) {
        // console.log ('resize canvas handler', this.launched, screenWidth, screenHeight);

        this.screenArea.width = screenWidth;
        this.screenArea.height = screenHeight;
        this.viewArea.width = viewWidth;
        this.viewArea.height = viewHeight;

        this.emit (AppEvent.RESIZED, AppEvent.RESIZED);
    }
    getLocationID () {
        return this.locationKey;
    }
    getScreenArea () {
        return this.screenArea.clone ();
    }
    getViewArea () {
        return this.viewArea.clone ();
    }
    getGrids () {
        return this.grids [Symbol.iterator] ();
    }
    getUnits () {
        return this.units [Symbol.iterator] ();
    }
    getUnitsNum () {
        return this.units.length;
    }
    getInputEnabled () {
        return this.inputEnabled;
    }
    getGridsStartPoint () {
        switch (this.locationKey) {
            case UIKeys.MAIN_LOCATION:
                return new PIXI.Point (80, 398);
            default:
                return new PIXI.Point (0, 0);
        }
    }
    selectUnit (id) {
        this.selectedUnit = this.unitsHashTable [id];
    }
    switchUnit (next=true) {
        if (this.units.length === 0) return;

        if (!this.selectedUnit) {
            this.selectedUnit = this.units [0];
        } else {
            this.selectedUnit.stopMoving ();

            let selectedInd = this.units.indexOf (this.selectedUnit);
            if (next) {
                selectedInd++;
                if (selectedInd > (this.units.length-1)) selectedInd = 0;
            } else {
                selectedInd--;
                if (selectedInd < 0) selectedInd = this.units.length-1;
            }
            this.selectedUnit = this.units [selectedInd];
            this.emit (AppEvent.UNITS_SELECTED, AppEvent.UNITS_SELECTED);
        }
    }
    getSelectedUnit () {
        return this.selectedUnit;
    }
    getUnitByID (id) {
        return this.unitsHashTable [id];
    }
    walkSelectedUnit (direction) {
        this._walkSelectedUnit (this.selectedUnit, direction);
    }
    _walkSelectedUnit (unit, direction) {
        if (!unit) return;
        unit.setWalkDirection (direction);
        this._checkUnitsDirections ();
    }

    getUnitsToUpdate () {
        return (this.unitsToUpdate.length > 0) ? this.unitsToUpdate [Symbol.iterator] () : null;
    }

    getConfig () {
        return this.config;
    }
    setDrawGrid (bool) {
        const updated = this.config.setDrawGrids (bool);
        if (updated) this.emit (AppEvent.CONFIG_UPDATED, AppEvent.CONFIG_UPDATED);
    }
    setGridAlign (bool) {
        const updated = this.config.setGridAlign (bool);
        if (updated) this.emit (AppEvent.CONFIG_UPDATED, AppEvent.CONFIG_UPDATED);
    }

    reset () {
        this.emit (AppEvent.GAME_CLEARED, AppEvent.GAME_CLEARED);
    }
    launch (scenarioKey=ScenarioKeys.REGULAR) {
        if (this.launched) return;
        this.launched = true;

        const voFactory = this.voFactory;

        switch (scenarioKey) {
/*
            case 'test-scenario':
                this.locationKey = UIKeys.MAIN_LOCATION;
                this._addGrid (voFactory, 10, 15);
                this._addUnit (voFactory, UnitTypes.REGULAR_UNIT, 'test-unit');
                this.selectUnit ('test-unit');
                this.inputEnabled = true;
                break;
*/
            case ScenarioKeys.NO_GRASS:
            case ScenarioKeys.WORK_DAY:
                this.locationKey = UIKeys.MAIN_LOCATION;

                this._addGrid (voFactory, 60, 5);
                this._addGrid (voFactory, 10, 50, 25, 5);
                this._addGrid (voFactory, 5, 50, 0, 5);
                this._addGrid (voFactory, 5, 50, 55, 5);
                this._addGrid (voFactory, 60, 5, 0, 55);
                this._addGrid (voFactory, 20, 10, 5, 25);

                if (scenarioKey === ScenarioKeys.NO_GRASS) {
                    // this._addUnit (voFactory, UnitTypes.REGULAR_UNIT, 'test-unit', 3, 4);
                    // this._addUnit (voFactory, UnitTypes.REGULAR_UNIT, 'test-unit', 16, 26);
                    this._addUnit (voFactory, UnitTypes.REGULAR_UNIT, 'boss');
                    this.selectUnit ('boss');
                } else {
                    this._addUnit (voFactory, UnitTypes.REGULAR_UNIT, 'boss');
                    this._addUnit (voFactory, UnitTypes.FAST_UNIT);
                    this._addUnit (voFactory, UnitTypes.SLOW_UNIT);
                    this._addUnit (voFactory, UnitTypes.TOO_SLOW_UNIT);
                    this._addUnit (voFactory, UnitTypes.REGULAR_UNIT);
                    this._addUnit (voFactory, UnitTypes.FAST_UNIT);
                    this._addUnit (voFactory, UnitTypes.SLOW_UNIT);
                    this._addUnit (voFactory, UnitTypes.TOO_SLOW_UNIT);
                    this._addUnit (voFactory, UnitTypes.REGULAR_UNIT);
                    this._addUnit (voFactory, UnitTypes.FAST_UNIT);
                    this._addUnit (voFactory, UnitTypes.SLOW_UNIT);
                    this._addUnit (voFactory, UnitTypes.TOO_SLOW_UNIT);

                    this.selectUnit ('boss');
                }

                this.inputEnabled = true;
                break;
            case ScenarioKeys.ALL_TOGETHER:
                this.locationKey = UIKeys.MAIN_LOCATION;
                this._addGrid (voFactory, 60, 60);

                this._addUnit (voFactory, UnitTypes.REGULAR_UNIT, 'boss');
                this._addUnit (voFactory, UnitTypes.FAST_UNIT);
                this._addUnit (voFactory, UnitTypes.SLOW_UNIT);
                this._addUnit (voFactory, UnitTypes.TOO_SLOW_UNIT);
                this._addUnit (voFactory, UnitTypes.REGULAR_UNIT);
                this._addUnit (voFactory, UnitTypes.FAST_UNIT);
                this._addUnit (voFactory, UnitTypes.SLOW_UNIT);
                this._addUnit (voFactory, UnitTypes.TOO_SLOW_UNIT);
                this._addUnit (voFactory, UnitTypes.REGULAR_UNIT);
                this._addUnit (voFactory, UnitTypes.FAST_UNIT);
                this._addUnit (voFactory, UnitTypes.SLOW_UNIT);
                this._addUnit (voFactory, UnitTypes.TOO_SLOW_UNIT);

                this.selectUnit ('boss');
                this.inputEnabled = true;
                break;
            case ScenarioKeys.REGULAR: // just to remind that default scenario - is REGULAR
            default:
                this.locationKey = UIKeys.MAIN_LOCATION;
                this._addGrid (voFactory, 60, 60);
                // this._addUnit (voFactory, UnitTypes.REGULAR_UNIT, 'test-unit', 2, 2);
                this._addUnit (voFactory, UnitTypes.REGULAR_UNIT, 'boss');
                this.selectUnit ('boss');
                this.inputEnabled = true;
        }

        this.emit (AppEvent.GAME_LAUNCHED, AppEvent.GAME_LAUNCHED);
    }
    addRandomUnit () {
        if (this.units.length >= this.config.maxUnits) return;
        const unitTypes = [UnitTypes.REGULAR_UNIT, UnitTypes.FAST_UNIT, UnitTypes.SLOW_UNIT, UnitTypes.TOO_SLOW_UNIT];
        const rand = Math.round (Math.random() * (unitTypes.length-1));
        const newUnitID = this._addUnit (this.voFactory, unitTypes [rand]);
        this.emit (AppEvent.UNIT_ADDED, AppEvent.UNIT_ADDED, newUnitID);
        if (!this.selectedUnit) this.switchUnit ();
    }
    removeRandomUnit () {
        if (this.units.length === 0) return;
        const rand = Math.round (Math.random() * (this.units.length-1));
        const removedUnitID = this.units [rand].getID ();
        const bool = this._removeUnit (removedUnitID);
        if (bool) {
            this.emit (AppEvent.UNIT_REMOVED, AppEvent.UNIT_REMOVED, removedUnitID);
        }
    }

    _addUnit (voFactory, type, id, startColumn=-1, startRow=-1) {
        const unitVO = voFactory.getUnitVO ({type:type, id:id, config:this.config});
        if (startColumn > -1 && startRow > -1) {
            unitVO.setPosition (startColumn, startRow);
        } else {
            const randomPosition = this._getGridRandomPoint ();
            if (randomPosition) {
                unitVO.setPosition (randomPosition.column, randomPosition.row);
            }
        }
        this.units.push (unitVO);
        this.unitsHashTable [unitVO.getID ()] = unitVO;
        return unitVO.getID();
    }
    _removeUnit (id) {
        const unitVO = this.unitsHashTable [id];
        if (!unitVO) {
            return false;
        }
        const unitInd = this.units.indexOf (unitVO);
        this.units.splice (unitInd, 1);
        delete this.unitsHashTable [unitVO.getID ()];

        if (this.selectedUnit === unitVO) {
            this.selectedUnit = null;
            this.switchUnit ();
        }

        return true;
    }
    _addGrid (voFactory, columns, rows, startColumn=0, startRow=0) {
        const gridVO = voFactory.getGridVO ({columns:columns, rows:rows, startColumn:startColumn, startRow:startRow});
        this.grids.push (gridVO);
    }
    _checkUnitsDirections () {
        let un = this.units.length, unit;
        let foundDirection = false;
        while (--un >= 0) {
            unit = this.units [un];
            foundDirection = unit.getWalkDirection() !== null;
            if (foundDirection) break;
        }

        if (foundDirection && !this.ticking) {
            // console.log ("ticker starts...", this.appTicker);
            this.appTicker.add (this._pixiLoop, this);
            this.ticking = true;
        } else if (!foundDirection && this.ticking) {
            // console.log ("ticker stops...");
            this.appTicker.remove (this._pixiLoop, this);
            this.ticking = false;
        }
    }
    _pixiLoop (delta) {
        // console.log (`loop... ${delta}`);

        this.unitsToUpdate.length = 0;

        let un = this.units.length, unit, bool;
        while (--un >= 0) {
            unit = this.units [un];
            // bool = unit.updateWalk (this.getGrids (), delta);
            // bool = unit.updateWalk (this._searchGridPoint, this, delta);
            bool = unit.updateWalk (this._searchGridPoint.bind (this), delta);
            if (bool) {
                this.unitsToUpdate.push (unit);
            }
        }
        // console.log (`loop... ${this.unitsToUpdate.length}`);
        if (this.unitsToUpdate.length > 0) {
            this.emit (AppEvent.UNITS_MOVE, AppEvent.UNITS_MOVE);
        }
    }
    _getGridRandomPoint () {
        if (this.grids.length === 0) return null;
        const randGrid = this.grids [Math.round (Math.random() * (this.grids.length-1))];
        return randGrid.getRandomPoint ();
    }
    _searchGridPoint (column, row) {
        const gridsIterator = this.getGrids();
        let grid = gridsIterator.next();
        let gridVO, pointVO;
        while (!grid.done) {
            gridVO = grid.value;
            pointVO = gridVO.getPoint (column, row);
            if (pointVO) return pointVO;
            grid = gridsIterator.next();
        }
        return null;
    }

    toString () {
        return `[LocationModel]`;
    }
}

