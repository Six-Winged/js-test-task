/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/29/18 10:36 AM
 */

/*
Value object for field cells
 */
export class GridPointVO {

    constructor (col, row) {
        this._col = col;
        this._row = row;
    }

    get column () {
        return this._col;
    }
    get row () {
        return this._row;
    }
    clone () {
        return new GridPointVO (this._col, this._row);
    }

    toString () {
        return `[GridPoint column=${this._col} row=${this._row}]`;
    }

}
