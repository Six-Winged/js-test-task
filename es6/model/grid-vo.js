/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/28/18 8:57 PM
 */

import {VOFactory} from "../vo-factory.js";

/*
Value object of rectangular field grid, holds/returns 'GridPointVO' objects
 */
export class GridVO {

    constructor (columns, rows, startColumn, startRow) {
        this._pointsHashTable = {};
        this._pointsTable = [];
        this._columns = columns;
        this._columns = columns;
        this._rows = rows;
        this._startColumn = startColumn;
        this._startRow = startRow;
    }

    getPoint (column, row) {
        if (this._pointsTable.length > 0) {
            const pointKey = getPointHashKey (column, row);
            return this._pointsHashTable [pointKey];
        } else {
            if (column < this._startColumn || column >= (this._columns+this._startColumn)) return null;
            else if (row < this._startRow || row >= (this._rows+this._startRow)) return null;
            else return generatePoint (column, row);
        }
    }
    getRandomPoint () {
        let ret;
        if (this._pointsTable.length > 0) {
            ret = this._pointsTable [Math.round (Math.random () * (this._pointsTable.length-1))];
            ret = ret.clone ();
        } else {
            const minColumn = this._startColumn;
            const maxColumn = this._columns+this._startColumn-1;
            const randColumn = Math.round ((maxColumn - minColumn) * Math.random() + minColumn);
            const minRow = this._startRow;
            const maxRow = this._rows+this._startRow-1;
            const randRow = Math.round ((maxRow - minRow) * Math.random() + minRow);
            ret = this.getPoint (randColumn, randRow);
        }
        return ret;
    }

    /*
    method creates points objects array on demand only,
    when drawing rid requested, for example;
    otherwise we don't need to keep array of grid points,
    since we have square grids only and points can be calculated
     */
    getPoints () {
        if (this._pointsTable.length > 0) return;

        let c, r = -1, pointKey, pointVO;
        while (++r < this._rows) {
            c = -1;
            while (++c < this._columns) {
                pointKey = getPointHashKey (c+this._startColumn, r+this._startRow);
                pointVO = generatePoint (c+this._startColumn, r+this._startRow);
                this._pointsHashTable [pointKey] = pointVO;
                this._pointsTable.push (pointVO);
            }
        }


        return this._pointsTable [Symbol.iterator] ();
    }
    toString () {
        return `[GridVO points-num=${this._columns * this._rows}]`;
    }

}

let voFactory;
function generatePoint (column, row) {
    if (!voFactory) voFactory = new VOFactory;
    return voFactory.getGridPoint ({column:column, row:row});
}
function getPointHashKey (column, row) {
    return `point-${column}-${row}`;
}