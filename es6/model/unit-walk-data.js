/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/28/18 10:11 PM
 */

import {WalkDirections} from "../constants/walk-directions.js";
import {LocationHelper} from "../location-helper.js";

/*
Keeps details of game unit (UnitVO) movements
 */
export class UnitWalkData {

    constructor (targetColumn, targetRow, middlePoint, direction, pathPortion=1) {
        this._column = -1;
        this._row = -1;
        this._targetRow = targetRow;
        this._targetColumn = targetColumn;
        this._targetRow = targetRow;
        this._middlePoint = middlePoint;
        this._direction = direction;
        this._pathPortion = pathPortion;
        this._pendingDirection = false;
    }
    setPosition (column, row) {
        this._column = column;
        this._row = row;
    }
    get column () {
        return this._column;
    }
    get row () {
        return this._row;
    }

    checkCurrentCase (requestedDirection) {
        // console.log (`UnitWalkData check current state: ${requestedDirection} ${this._direction}`);

        if (requestedDirection === WalkDirections.STOPS || requestedDirection === WalkDirections.FIXING) {
            // finishing walk, nothing to change
            this._pendingDirection = null;
            return requestedDirection;
        }

        if (requestedDirection === this._direction) {
            // if directions are similar - keep requested
            // console.log (`...same directions`);
            return requestedDirection;
        } else if (LocationHelper.areDirectionsSimilar (requestedDirection, this._direction)) {
            // if directions are similar - keep walking but pending new direction
            // console.log (`...directions are similar: ${requestedDirection} / ${this._direction}`);
            this._pendingDirection = requestedDirection;
            return this._direction;
        } else if (LocationHelper.areDirectionsOpposite (requestedDirection, this._direction)) {
            // if directions are opposite - change direction to opposite
            // console.log (`...directions are opposite: ${requestedDirection} / ${this._direction}`);
            [this._column, this._row, this._targetColumn, this._targetRow] = [this._targetColumn, this._targetRow, this._column, this._row];
            this._direction = requestedDirection;
            return requestedDirection;
        } else {
            // if directions are different - keep going to nearest point and pending new direction
            // console.log (`...directions are different: ${requestedDirection} / ${this._direction}`);
            if (this._pathPortion <= .5) {
                [this._column, this._row, this._targetColumn, this._targetRow] = [this._targetColumn, this._targetRow, this._column, this._row];
                this._pendingDirection = requestedDirection;
                return LocationHelper.getOppositeDirection (this._direction);
            } else {
                this._pendingDirection = requestedDirection;
                return this._direction;
            }
        }
    }

    get middlePoint () {
        return (this._middlePoint) ? this._middlePoint.clone () : null;
    }
    get direction () {
        return this._direction;
    }
    resetPendingDirection () {
        this._pendingDirection = null;
    }
    get pendingDirection () {
        return this._pendingDirection;
    }
    isEmpty () {
        return this._targetColumn === -1 || this._targetRow === -1 || !this._middlePoint || !this._direction;
    }
    reset () {
        this._targetColumn = -1;
        this._targetRow = -1;
        this._middlePoint = null;
        this._direction = null;
        this._pathPortion = 0;
    }

    set (targetColumn, targetRow, middlePoint, direction, pathPortion) {
        this._targetColumn = targetColumn;
        this._targetRow = targetRow;
        this._middlePoint = (middlePoint) ? middlePoint.clone () : null;
        this._direction = direction;
        this._pathPortion = pathPortion;
    }
    clone () {
        const mpoint = (this._middlePoint) ? this._middlePoint.clone () : null;
        return new UnitWalkData (this._targetColumn, this._targetRow, mpoint, this._direction);
    }

    toString () {
        let str = `[UnitWalkData column=${this._targetColumn} row=${this._targetRow} direction='${this._direction}' middle-point=`;
        str += (this._middlePoint) ? `(${this._middlePoint.x}, ${this._middlePoint.y})]` : `null]`;
        return str;
    }

}
