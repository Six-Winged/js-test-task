/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/26/18 8:50 PM
 */

/*
    Application start point, here we are:

    - load game assets;
    - create PIXI application;
    - handle for browser resize events;
    - launch the game.
 */

"use strict";

import pixi from "../libs/pixi.min.js";
import TweenLite from "../libs/TweenLite.min.js";
import {AssetsKeys} from "./constants/assets-keys.js";
import {UIFactory} from "./ui-factory.js";
import {LocationModel} from "./model/location-model.js";
import {LocationController} from "./controller/location-controller.js";
import {LocationConfig} from "./location-config.js";
import {render} from 'inferno';
import GameHUD from "./view/inferno/GameHUD.js";

const gameViewRatio = .35;
let windowWidth = window.innerWidth;// * .75;
let windowHeight = window.innerHeight;// * .75;
let viewWidth = window.innerWidth * gameViewRatio;
let viewHeight = window.innerHeight * gameViewRatio;

const canvas = document.getElementById ('game-canvas');
const walkingApp = new PIXI.Application ({backgroundColor:0xaaaaaa, width:windowWidth, height:windowHeight, view:canvas});
walkingApp.renderer.autoResize = true;

document.body.style.backgroundColor = '#303030';
document.body.style.margin = "0px";
document.body.style.padding = "0px";

window.addEventListener ('resize', function (event) {
    windowWidth = window.innerWidth;
    windowHeight = window.innerHeight;

    viewWidth = window.innerWidth * gameViewRatio;
    viewHeight = window.innerHeight * gameViewRatio;

    walkingApp.view.width = windowWidth;
    walkingApp.view.height = windowHeight;

    walkingApp.renderer.resize (windowWidth, windowHeight);

    if (locationModel) {
        locationModel.resizeCanvas (windowWidth, windowHeight, viewWidth, viewHeight);
    }
});

walkingApp.view.style.margin = '0px';
walkingApp.view.style.marginLeft = (window.innerWidth - walkingApp.screen.width) / 2 + 'px';
walkingApp.view.style.marginTop = (window.innerHeight - walkingApp.screen.height) / 2 + 'px';

walkingApp.view.style.padding = "0px";
walkingApp.view.style.display = "block";

const assetsLoader = PIXI.loader;

let locationConfig, locationModel, locationController, locationView;

function loadingHandler (loader, resources) {
    // console.log (`loading handler...`);
    launchGame ();

}
function launchGame () {
    const urlParams = new URLSearchParams (window.location.href);
    const drawGrid = urlParams.get ('draw-grid') === '1';
    const gridAlign = urlParams.get ('grid-align') === '1';
    const scenarioKey = urlParams.get ('scenario');
    const uiFactory = new UIFactory;

    // locationConfig = new LocationConfig (false, 21, 12, true);
    locationConfig = new LocationConfig (gridAlign, 21, 12, drawGrid);

    // const scenarioKey = 'test-scenario';
    // locationConfig = new LocationConfig (false, 63, 36, true);

    locationModel = new LocationModel (locationConfig, walkingApp.ticker, windowWidth, windowHeight, viewWidth, viewHeight);
    locationController = new LocationController (locationModel);
    locationView = uiFactory.getLocation (locationModel, locationController, locationConfig, walkingApp.ticker);

    walkingApp.stage.addChild (locationView);

    // inferno navigation
    const gameHUD = <GameHUD model={locationModel} controller={locationController} gameConfig={locationConfig} />;
    render (gameHUD, document.getElementById ('inferno-nav'));

    locationModel.launch (scenarioKey);
}

assetsLoader.add (AssetsKeys.MAIN_LAND, './assets/land.png')
            .add (AssetsKeys.WALKING_WOMAN, './assets/wom_sheet/2.json')
            .add (AssetsKeys.UNIT_SELECTIONS, './assets/unit-selections/unit-selections.json')
            .load (loadingHandler);

