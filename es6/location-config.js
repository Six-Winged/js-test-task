/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/27/18 8:43 PM
 */

import {UnitTypes} from "./constants/unit-types.js";
import {WalkDirections} from "./constants/walk-directions.js";

/*
Class contains basic game options
 */
export class LocationConfig {

    constructor (gridAlign=false, tileWidth=21, tileHeight=12, drawGrids=false) {
        this._gridAlign = gridAlign;
        this._tileWidth = tileWidth;
        this._tileHeight = tileHeight;
        this._drawGrids = drawGrids;
    }

    /*
    Unit's speed (depending on it's type, declared by UnitTypes class)
     */
    getAnimationSpeed (direction, unitType=UnitTypes.REGULAR_UNIT) {
        let speed, speedK = 1;
        switch (unitType) {
            case UnitTypes.REGULAR_UNIT:
                speed = 1;
                break;
            case UnitTypes.FAST_UNIT:
                speed = 1.4;
                break;
            case UnitTypes.SLOW_UNIT:
                speed = .65;
                break;
            case UnitTypes.TOO_SLOW_UNIT:
                speed = .35;
                break;
        }
        switch (direction) {
            case WalkDirections.WEST:
            case WalkDirections.EAST:
                speedK = 1;
                break;
            case WalkDirections.SOUTH:
            case WalkDirections.NORTH:
                speedK = .57;
                break;
            case WalkDirections.SOUTH_EAST:
            case WalkDirections.SOUTH_WEST:
            case WalkDirections.NORTH_EAST:
            case WalkDirections.NORTH_WEST:
                speedK = .67;
                break;
        }
        return speed * speedK;
    }

    /*
    Maximal units available to add
     */
    get maxUnits () {
        return 20;
    }
    /*
    Size of game field cell;
     */
    get tileWidth () {
        return this._tileWidth;
    }
    get tileHeight () {
        return this._tileHeight;
    }
    /*
    Whether we need to draw field grid, for test purposes;
     */
    setDrawGrids (bool) {
        if (this._drawGrids === bool) return false;
        this._drawGrids = bool;
        return true;
    }
    get drawGrids () {
        return this._drawGrids;
    }
    /*
    Whether game unit must finish it's movement on strict field's cell
    or it can stop in half-way
     */
    setGridAlign (bool) {
        if (this._gridAlign === bool) return false;
        this._gridAlign = bool;
        return true;
    }
    get gridAlign () {
        return this._gridAlign;
    }

}
