/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/26/18 8:51 PM
 */

import {LocationView} from "./view/location-view.js";
import {UIKeys} from "./constants/ui-keys.js";
import {AssetsKeys} from "./constants/assets-keys.js";
import {WalkingUnit} from "./view/walking-unit.js";

/*
Factory used to create visual game elements
 */
export class UIFactory {

    getUnitSelection (key) {
        let framesSet;
        switch (key) {
            case UIKeys.UNIT_SELECTION_WHITE:
                framesSet = getFramesSet ('unit-selection-white{n}.png', 50, 1);
                break;
            case UIKeys.UNIT_SELECTION_YELLOW:
                framesSet = getFramesSet ('unit-selection-yellow{n}.png', 50, 1);
                break;
            case UIKeys.UNIT_SELECTION_BLUE:
                framesSet = getFramesSet ('unit-selection-blue{n}.png', 50, 1);
                break;
            case UIKeys.UNIT_SELECTION_ZOMBIE:
                framesSet = getFramesSet ('unit-selection-zombie{n}.png', 50, 1);
                break;
        }
        return (framesSet && framesSet.length > 0) ? new PIXI.extras.AnimatedSprite (framesSet) : null;
    }
    getUnit (key, type, id) {
        let ret;
        let northFrames, southFrames, eastFrames, westFrames, northEastFrames, northWestFrames, southEastFrames, southWestFrames, walkingUnit;
        switch (key) {
            case UIKeys.WALKING_WOMAN:
                northFrames = getFramesSet ('woman_walk_def_N_{n}.png');
                southFrames = getFramesSet ('woman_walk_def_S_{n}.png');
                eastFrames = getFramesSet ('woman_walk_def_E_{n}.png');
                westFrames = getFramesSet ('woman_walk_def_W_{n}.png');
                northEastFrames = getFramesSet ('woman_walk_def_NE_{n}.png');
                northWestFrames = getFramesSet ('woman_walk_def_NW_{n}.png');
                southEastFrames = getFramesSet ('woman_walk_def_SE_{n}.png');
                southWestFrames = getFramesSet ('woman_walk_def_SW_{n}.png');
                ret = new WalkingUnit (id,
                                       type,
                                       {northFrames:northFrames,
                                       southFrames:southFrames,
                                       eastFrames:eastFrames,
                                       westFrames:westFrames,
                                       northEastFrames:northEastFrames,
                                       northWestFrames:northWestFrames,
                                       southWestFrames:southWestFrames,
                                       southEastFrames:southEastFrames});
                break;
        }
        return ret;
    }
    getLocationBackground (key) {
        let ret;
        switch (key) {
            case UIKeys.MAIN_LOCATION:
                ret = PIXI.Texture.fromImage (AssetsKeys.MAIN_LAND);
                break;
        }
        return ret;
    }
    getLocation (model, controller, config, ticker) {
        return new LocationView (model, controller, config, ticker);
    }
}

function formatFrameNumber (frame, length) {
    const frameString = String (frame);
    if (frameString.length > length) {
        return frameString.substr (0, length);
    } else {
        return '0'.repeat (length-frameString.length) + frame;
    }
}
function getFramesSet (prefix, framesNum=40, startIndex=0) {
    let ret = [];
    let frameObj, fr = startIndex-1, frameName;
    while (++fr < framesNum) {
        frameName = prefix.replace ('{n}', formatFrameNumber (fr, 4));
        try {
            frameObj = PIXI.Texture.fromFrame (frameName);
            ret.push (frameObj);
        } catch (err) {
            break;
        }
    }
    return ret;
}
