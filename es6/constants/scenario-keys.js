/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/28/18 9:00 PM
 */

/*
Predetermined scenarios keys
 */
export class ScenarioKeys {

    static get REGULAR () { return 'regular'; }
    static get NO_GRASS () { return 'no-grass'; }
    static get WORK_DAY () { return 'work-day'; }
    static get ALL_TOGETHER () { return 'all-together'; }

}
