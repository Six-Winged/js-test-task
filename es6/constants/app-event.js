/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/27/18 9:55 AM
 */

/*
Event types for application model emits
 */
export class AppEvent {

    static get UNIT_ADDED () { return 'app-event-unit-added'; }
    static get UNIT_REMOVED () { return 'app-event-unit-removed'; }
    static get UNITS_MOVE () { return 'app-event-units-move'; }
    static get UNITS_SELECTED () { return 'app-event-units-selected'; }
    static get GAME_LAUNCHED () { return 'app-event-game-launched'; }
    static get CONFIG_UPDATED () { return 'app-event-config-updated'; }
    static get GAME_CLEARED () { return 'app-event-game-cleared'; }
    static get RESIZED () { return 'app-event-reeized'; }

}
