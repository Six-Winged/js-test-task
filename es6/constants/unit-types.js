/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/27/18 6:59 PM
 */

/*
Constants for creating game units with predetermined different parameters (for now it's just speed)
 */
export class UnitTypes {

    static get REGULAR_UNIT () { return 'unit-types-regular'; }
    static get FAST_UNIT () { return 'unit-types-quick'; }
    static get SLOW_UNIT () { return 'unit-types-slow'; }
    static get TOO_SLOW_UNIT () { return 'unit-types-too-slow'; }

}
