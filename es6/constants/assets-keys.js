/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/26/18 10:15 PM
 */

/*
Assets prefixes, to avoid typos
 */
export class AssetsKeys {

    static get MAIN_LAND () { return 'asset-key-main-land'; }
    static get WALKING_WOMAN () { return 'asset-key-walking-woman'; }
    static get UNIT_SELECTIONS () { return 'asset-key-unit-selections'; }

}
