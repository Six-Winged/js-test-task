/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/28/18 8:54 PM
 */

/*
Constants used to describe unit movement phases/directions
 */
export class WalkDirections {

    static get SOUTH () { return 'walk-direction-south'; }
    static get NORTH () { return 'walk-direction-north'; }
    static get EAST () { return 'walk-direction-east'; }
    static get WEST () { return 'walk-direction-west'; }
    static get NORTH_WEST () { return 'walk-direction-north-west'; }
    static get NORTH_EAST () { return 'walk-direction-north-east'; }
    static get SOUTH_WEST () { return 'walk-direction-south-west'; }
    static get SOUTH_EAST () { return 'walk-direction-south-east'; }

    static get STOPS () { return 'walk-direction-stops'; }
    static get FIXING () { return 'walk-direction-fixing'; }

}
