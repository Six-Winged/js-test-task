/*
 * Copyright (c) 2004-2018 Six-Winged.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alex T Seraphim <alex.t.seraphim@gmail.com>, 8/26/18 8:53 PM
 */

/*
Keys used in UIFactory to create visual game elements
 */
export class UIKeys {

    static get MAIN_LOCATION () { return 'ui-key-main-location'; }
    static get WALKING_WOMAN () { return 'ui-key-walking-woman'; }
    static get UNIT_SELECTION_WHITE () { return 'ui-key-unit-selection-white'; }
    static get UNIT_SELECTION_YELLOW () { return 'ui-key-unit-selection-yellow'; }
    static get UNIT_SELECTION_BLUE () { return 'ui-key-unit-selection-blue'; }
    static get UNIT_SELECTION_ZOMBIE () { return 'ui-key-unit-selection-zombie'; }

    static get NEXT_UNIT () { return 'ui-key-next-unit'; }
    static get PREV_UNIT () { return 'ui-key-prev-unit'; }
    static get ADD_UNIT () { return 'ui-key-add-unit'; }
    static get REMOVE_UNIT () { return 'ui-key-remove-unit'; }
    static get SHOW_HIDE_GRID () { return 'ui-key-show-hide-grid'; }
    static get UNIT_ALIGN () { return 'ui-key-unit-align'; }

}
